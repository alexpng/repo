#!/bin/bash

echo [][][][][][][][][][][][][][][]
echo 
echo !————————IMPORTANT——————————!
echo 
echo This will delete all the custom wallpapers you have made that are located in /Library/WallpaperLoader/DarkPapersCustom
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo !————————IMPORTANT——————————!
echo 
echo This will delete all the custom wallpapers you have made that are located in /Library/WallpaperLoader/DarkPapersCustom
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo !————————IMPORTANT——————————!
echo 
echo This will delete all the custom wallpapers you have made that are located in /Library/WallpaperLoader/DarkPapersCustom
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo !————————IMPORTANT——————————!
echo 
echo This will delete all the custom wallpapers you have made that are located in /Library/WallpaperLoader/DarkPapersCustom
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo !————————IMPORTANT——————————!
echo 
echo This will delete all the custom wallpapers you have made that are located in /Library/WallpaperLoader/DarkPapersCustom
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo Deleting all custom wallpapers inside /Library/WallpaperLoader/DarkPapersCustom
echo 
echo Please wait...
echo 
echo 
rm -rfv /Library/WallpaperLoader/DarkPapersCustom/*
echo 
echo 
echo [][][][][][][][][][][][][][][]
echo 
echo !—————————COMPLETED—————————!
echo 
echo All custom wallpapers have been deleted. 
echo 
echo You DON’T need a Respring for the changes to take effect.
echo 
echo You can exit the script now.
echo 
echo [][][][][][][][][][][][][][][]